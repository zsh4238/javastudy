package com.zsh.dbassit;

import com.sun.org.apache.xpath.internal.objects.XObject;
import com.zsh.handler.IResultSetHandler;

import javax.sql.DataSource;
import java.sql.*;

/**
 * @author zsh
 * 通过c3p0-config.xml的链接管理机制创建一个连接器
 *
 */
public class Dbassit {
    private DataSource dataSource=null;

    public Dbassit(DataSource dataSource){
        this.dataSource=dataSource;
    }

    public Object query(String sql, IResultSetHandler  irs, Object... params){
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            //获取链接
            con=dataSource.getConnection();
            //使用sql创造预处理
            ps=con.prepareStatement(sql);
            //3.得到sql语句参数的源信息（有几个参数，都什么类型等等）
            ParameterMetaData pd=ps.getParameterMetaData();
            //4.判断语句中参数的个数和方法参数params的个数是否一致，不一致肯定没法执行
            int parameterCount = pd.getParameterCount();//参数的个数（问号的个数)
            if(parameterCount > 0) {
                if (params == null) {
                    throw new NullPointerException("没有sql语句执行必须的参数");
                }
                if (params.length != parameterCount) {
                    throw new RuntimeException("传入的参数个数和语句所需的参数个数不一致，语句无法执行");
                }
            }


            for (int i = 0; i <parameterCount ; i++) {
                ps.setObject(i+1,params[i]);
            }
            //6.执行语句
            rs = ps.executeQuery();
            //7.返回执行结果

            return irs.handle(rs);

        }catch (Exception e){
            throw new RuntimeException(e);
        }finally {
            relaseConn(con,ps,null);

        }


    }

    /**
     * 执行增删改的方法
     * @param sql
     * @param params
     * @return
     */
    public void update(String sql,Object...params){
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            //获取链接
            con=dataSource.getConnection();
            //使用sql创造预处理
            ps=con.prepareStatement(sql);
            //3.得到sql语句参数的源信息（有几个参数，都什么类型等等）
            ParameterMetaData pd=ps.getParameterMetaData();
            //4.判断语句中参数的个数和方法参数params的个数是否一致，不一致肯定没法执行
            int parameterCount = pd.getParameterCount();//参数的个数（问号的个数)
            if(parameterCount > 0) {
                if (params == null) {
                    throw new NullPointerException("没有sql语句执行必须的参数");
                }
                if (params.length != parameterCount) {
                    throw new RuntimeException("传入的参数个数和语句所需的参数个数不一致，语句无法执行");
                }
            }


            for (int i = 0; i <parameterCount ; i++) {
                ps.setObject(i+1,params[i]);
            }
            //6.执行语句
            int res = ps.executeUpdate();
            //7.返回执行结果
            System.out.println(res);;
        }catch (Exception e){
            throw new RuntimeException(e);
        }finally {
            relaseConn(con,ps,null);

        }



    }

    /**
     * 释放连接资源
     * @param con
     * @param ps
     * @param rs
     */
    public void  relaseConn(Connection con,PreparedStatement ps,ResultSet rs){
       try {
           if (con!=null){
               con.close();
           }
           if (ps!=null)
           {
               ps.close();
           }
           if (rs!=null){
               rs.close();
           }
       }catch (RuntimeException | SQLException e){
           e.printStackTrace();
       }
    }

}
