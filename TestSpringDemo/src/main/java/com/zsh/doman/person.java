package com.zsh.doman;

public class person {
    private  int id;
    private  String usrname;


    @Override
    public String toString() {
        return "person{" +
                "id=" + id +
                ", usrname='" + usrname + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsrname() {
        return usrname;
    }

    public void setUsrname(String usrname) {
        this.usrname = usrname;
    }
}
