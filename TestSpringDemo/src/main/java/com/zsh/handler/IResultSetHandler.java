package com.zsh.handler;

import java.sql.ResultSet;

public interface IResultSetHandler {
    /**
     * 结果集封装的方法
     * @param rs
     * @return
     */
    Object  handle(ResultSet rs);
   // Object listHandler(ResultSet re);

}
