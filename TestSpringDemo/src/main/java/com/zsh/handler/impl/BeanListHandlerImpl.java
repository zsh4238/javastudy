package com.zsh.handler.impl;

import com.zsh.handler.IResultSetHandler;
import javafx.beans.binding.ObjectExpression;
import sun.plugin.com.PropertySetDispatcher;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class BeanListHandlerImpl implements IResultSetHandler {
    //创建一个domain类，用来反射参数bean！
    private Class daomainClass;

    //配置构造函数！
    public BeanListHandlerImpl(Class daomainClass) {
        this.daomainClass = daomainClass;
    }


    /**
     * 传递一个结果集，返回一个list
     *
     * @param rs
     * @return
     */
    @Override
    public Object handle(ResultSet rs) {
        try {
            List<Object> list = new ArrayList<Object>();
            //遍历结果集
            while (rs.next()) {
                Object bean = daomainClass.newInstance();
                ResultSetMetaData resultSetMetaData = rs.getMetaData();
                int num = resultSetMetaData.getColumnCount();
                for (int i = 1; i <= num; i++) {
                    //根据字段的顺序获得相应的参数
                    String columName = resultSetMetaData.getColumnName(i);
                    //列名其实就是实体类的属性名称，于是就可以使用列名得到实体类中属性的描述器
                    //实体类中定义的私有类成员和它的get以及set方法
                    PropertyDescriptor pd = new PropertyDescriptor(columName, daomainClass);
                    //获得写入的方法
                    Method writeMethod = pd.getWriteMethod();
                    //根据字段名字获取对应的值
              Object colum = rs.getObject(columName);
                    writeMethod.invoke(bean, colum);
                    list.add(bean);
                }

            }
            //返回一个列表，需要传递回去的值，元素为对象！
            return list;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
