package com.zsh.service;

import com.zsh.dao.Util;
import javafx.application.Application;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;

public class impService {
    public static void main(String[] args) throws SQLException {

        ApplicationContext ac=new ClassPathXmlApplicationContext("Bean.xml");
        Util ut= (Util) ac.getBean("util");
        ut.conMysql();
    }
}
