package com.zsh.util;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author zsh
 * 定义一个连接
 */
public class C3P0Util {

    private  static  ComboPooledDataSource cd = new ComboPooledDataSource();

    public static ComboPooledDataSource getCd() {
        return cd;
    }

    public static Connection getConnection() throws SQLException {
        return cd.getConnection();
    }
}
